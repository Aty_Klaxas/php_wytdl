<?php declare(strict_types=1);

require_once __DIR__ . '/../../private/cookie_anti_flood.php';
require_once __DIR__ . '/../../private/autoload.php';

use GearmanJobs\Download;
use GearmanJobs\GearmanHelper;
use src\Enum\QualitySimpleSelection;
use src\Enum\TypeSimpleSelection;
use src\Response;
use src\Enum\GearmanStatus;

$Response = new Response();
$Link = parameter_url_to_link();

$all_couples = [
    [QualitySimpleSelection::LOW(), TypeSimpleSelection::AUDIO()],
    [QualitySimpleSelection::HIGH(), TypeSimpleSelection::AUDIO()],
    [QualitySimpleSelection::DEFAULT(), TypeSimpleSelection::VIDEO()],
    [QualitySimpleSelection::LOW(), TypeSimpleSelection::VIDEO()],
    [QualitySimpleSelection::MEDIUM(), TypeSimpleSelection::VIDEO()],
    [QualitySimpleSelection::HIGH(), TypeSimpleSelection::VIDEO()],
];

$keys = array_map(static function($a_couple) {
    return $a_couple[0]->getValue() .'_'. $a_couple[1]->getValue();
}, $all_couples);

$all_couples = array_combine($keys, $all_couples);

$tasks = array_map(static function($a_couple) use ($Link) {
    return Download::getTask($Link->getUrlString(), $a_couple[0], $a_couple[1]);
}, $all_couples);

$status_array = GearmanHelper::getStatus($tasks);

$type_n_quality = array_map(static function($a_couple) use ($Link, $status_array) {
    $status = $status_array[$a_couple[0]->getValue() .'_'. $a_couple[1]->getValue()] ?? null;

    if ($status instanceof GearmanStatus) {
        $status = $status->getValue();
    }

    return [
        'status' => $status,
        'is_downloaded' => $Link->isVideoMp4Downloaded($a_couple[0], $a_couple[1]),
        'download_link' => $Link->getVideoMp4DownloadLink($a_couple[0], $a_couple[1]),
    ];
}, $all_couples);

$Response->send_ok_n_data_n_exit([
    'real_url' => $Link->getUrlString(),
    'hash' => $Link->getHash(),
    'have_info' => $Link->haveInfo(),
    'message' => $Link->haveInfo() ? 'ok' : "Need info for 'is_downloaded' or 'download_link'",
    'download_info' => $type_n_quality,
]);
