<?php declare(strict_types=1);

require_once __DIR__ . '/../../private/cookie_anti_flood.php';
require_once __DIR__ . '/../../private/autoload.php';

use GearmanJobs\GetInfo;
use src\Response;
use Wa72\Url\Url;
use src\Link;
use GearmanJobs\Download;
use src\Enum\GearmanStatus;
use src\Enum\QualitySimpleSelection;
use src\Enum\TypeSimpleSelection;

$Response = new Response();
$quality = new QualitySimpleSelection($_GET['quality'] ?? null);
$type = new TypeSimpleSelection($_GET['type'] ?? null);

if ($type->getValue() === null) {
    $Response->send_error_n_exit('ERROR: You must pass \'type\' parameter and it must be: ' . implode(',', TypeSimpleSelection::getAll()));
}

if ($quality->getValue() === null) {
    $quality->setValue(QualitySimpleSelection::DEFAULT);
}

$all_couples = [
    [QualitySimpleSelection::LOW(), TypeSimpleSelection::AUDIO()],
    [QualitySimpleSelection::HIGH(), TypeSimpleSelection::AUDIO()],
    [QualitySimpleSelection::DEFAULT(), TypeSimpleSelection::VIDEO()],
    [QualitySimpleSelection::LOW(), TypeSimpleSelection::VIDEO()],
    [QualitySimpleSelection::MEDIUM(), TypeSimpleSelection::VIDEO()],
    [QualitySimpleSelection::HIGH(), TypeSimpleSelection::VIDEO()],
];

/** @var array{0: QualitySimpleSelection, 1: TypeSimpleSelection} $filter_by_type */
$filter_by_type = array_filter($all_couples, static function($a_couple) use ($type) {
    return $type->getValue() === $a_couple[1]->getValue();
});

/** @var array{0: QualitySimpleSelection, 1: TypeSimpleSelection} $filter_by_couple */
$filter_by_couple = array_filter($filter_by_type, static function($a_couple) use ($quality, $type) {
    return $quality->getValue() === $a_couple[0]->getValue();
});

if (empty($filter_by_couple)) {
    $qualities = '';

    if (!empty($filter_by_type)) {
        $qualities = ' (Usables qualities: ' . implode(',', array_map(static function($item) {
            return $item[0]->getValue();
        }, $filter_by_type)) . ')';
    }

    $Response->send_error_n_exit('ERROR: this couple of parameter \'quality\' + \'type\' is invalid (type: ' . ($type->getValue() ?? 'null') . ' quality: ' . ($quality->getValue() ?? 'null') . ')' . $qualities);
}

$Link = parameter_url_to_link();
$url = $Link->getUrlString();

// get info loop
if (!$Link->haveInfo()) {
    $timout = 0;
    $limit = 5;

    do {
        $task = GetInfo::getTask($Link->getUrlString());
        $status = getStatusOfATask($task);
        $haveInfo = $Link->haveInfo();

        if ($status->getValue() === GearmanStatus::UNKNOWN && !$haveInfo) {
            GetInfo::callJob($Link->getUrlString(), true);
        }

        if (!$haveInfo) {
            $timout++;
            sleep(3);
        }
    } while (!$haveInfo && $timout < $limit);

    if ($timout === $limit) {
        $Response->send_error_n_exit('Get in info timeout, please retry');
    }
}

// get task status
$task = Download::getTask($url, $quality, $type);
$status = getStatusOfATask($task);

// if not downloaded AND status say nothing
if (in_array($status->getValue(), [null, GearmanStatus::UNKNOWN], true) && !$Link->isVideoMp4Downloaded($quality, $type)) {
    // download it and update status
    $task = Download::callJob($url, $quality, $type, true);
    $status = getStatusOfATask($task);
}

// GearmanStatus to string
if ($status instanceof GearmanStatus) {
    $status = $status->getValue();
}

$Response->send_ok_n_data_n_exit([
    'quality' => $quality->getValue(),
    'type' => $type->getValue(),
    'status' => $status,
    'is_downloaded' => $Link->isVideoMp4Downloaded($quality, $type),
    'video_file' => $Link->getVideoMp4DownloadLink($quality, $type),
]);
