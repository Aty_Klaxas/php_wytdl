<?php declare(strict_types=1);

require_once __DIR__ . '/../../private/cookie_anti_flood.php';
require_once __DIR__ . '/../../private/autoload.php';

use src\Enum\GearmanStatus;
use src\Response;
use Wa72\Url\Url;
use src\Link;
use GearmanJobs\GetInfo;

$Response = new Response();
$Link = parameter_url_to_link();
$task = GetInfo::getTask($Link->getUrlString());
$status = getStatusOfATask($task);

if (in_array($status->getValue(), [null, GearmanStatus::UNKNOWN], true) && !$Link->haveInfo()) {
    $task = GetInfo::callJob($Link->getUrlString(), true);
    $status = getStatusOfATask($task);
}

$Response->send_ok_n_data_n_exit([
    'have_info' => $Link->haveInfo(),
    'status' => $status->getValue(),
    'info' => $Link->getInfoArray(),
]);
