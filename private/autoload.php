<?php

/*
 * Enable show debug
 */

if (isset($_GET['debug'])) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
} else {
    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
    error_reporting(0);
}

/*
 * Const and Uses
 */

const BASE_URL = 'SET_BASE_URL_PLEASE_IN_autoload_php';

const NET_GEARMAN_JOB_PATH = __DIR__ . '/../batch/jobs';
const NET_GEARMAN_JOB_CLASS_PREFIX = '';

/*
 * General
 */

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/../batch/jobs/GearmanHelper.php';
require_once __DIR__ . '/_functions.php';

/*
 * Gearman Jobs
 */

require_once __DIR__ . '/../batch/jobs/ParentJob.php';

$path_jobs = NET_GEARMAN_JOB_PATH;
$scan = scandir($path_jobs);

// get child jobs
foreach ($scan as $item) {
    if (in_array($item, ['.', '..', 'GearmanHelper.php', 'ParentJob.php'], true)) continue;

    require_once $path_jobs . '/' . $item;
}
