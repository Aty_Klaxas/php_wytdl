<?php declare(strict_types=1);

namespace src;

use League\CLImate\CLImate;
use src\Enum\DownloaderTierApiDownload;
use src\Enum\GearmanStatus;
use src\Enum\QualitySimpleSelection;
use src\Enum\TypeSimpleSelection;
use Wa72\Url\Url;
use GearmanJobs\GetInfo;

class DownloaderTierApi
{

    public const BIN = 'TODO_SET_DOWNLOADER_PATH_HERE';

    public static function getInfoDumpSingleJson(Link $link)
    {
        if (!is_dir(Link::PATH_INFO)) {
            return false;
        }

        $url = verify_and_re_verify_url($link->getUrlString());

        if (!$url instanceof Url) {
            return false;
        }

        make_dir($link->getInfoPath());
        $cmd = self::BIN . ' --dump-single-json ' . $link->getUrlString() . ' > ' . $link->getInfoPath();
        exec($cmd);

        return true;
    }

    /**
     * parameter to add to re-encode media in mp3/mp4
     *
     * @param TypeSimpleSelection $type
     * @return string|null
     */
    private static function download_get_parameter_re_encode(TypeSimpleSelection $type)
    {
        if ($type->getValue() === TypeSimpleSelection::VIDEO) return '--recode-video mp4';
        if ($type->getValue() === TypeSimpleSelection::AUDIO) return '-x --audio-format mp3';

        return null;
    }

    /**
     * The -f content
     *
     * @param TypeSimpleSelection $type
     * @return string|null
     */
    private static function download_get_format_parameter(TypeSimpleSelection $type, QualitySimpleSelection $quality)
    {
        if ($type->getValue() === TypeSimpleSelection::VIDEO) {
            if ($quality->getValue() === QualitySimpleSelection::DEFAULT) {
                return 'mp4';
            }

            if ($quality->getValue() === QualitySimpleSelection::LOW) {
                return 'wv[height<360]+wa';
            }

            if ($quality->getValue() === QualitySimpleSelection::LOW_PLUS) {
                return 'bv[height<360]+ba';
            }

            if ($quality->getValue() === QualitySimpleSelection::MEDIUM) {
                return 'bv[height=360]+ba';
            }

            if ($quality->getValue() === QualitySimpleSelection::HIGH) {
                return 'bv[height=1080]+ba';
            }

            if ($quality->getValue() === QualitySimpleSelection::VERY_HIGH) {
                return 'bv[height>=1080]+ba';
            }
        }

        if ($type->getValue() === TypeSimpleSelection::AUDIO) {
            if ($quality->getValue() === QualitySimpleSelection::HIGH) {
                return 'ba';
            }

            if ($quality->getValue() === QualitySimpleSelection::LOW) {
                return 'wa';
            }
        }

        return null;
    }

    public static function download(Link $link, QualitySimpleSelection $quality, TypeSimpleSelection $type, string $error_log_file, ?CLImate $CLImate = null): DownloaderTierApiDownload
    {
        $url = verify_and_re_verify_url($link->getUrlString());

        if (!$url instanceof Url) {
            return DownloaderTierApiDownload::INVALID_LINK();
        }

        $parameter_recode_video = self::download_get_parameter_re_encode($type);
        $parameter_format = self::download_get_format_parameter($type, $quality);

        if (empty($parameter_recode_video)) {
            return DownloaderTierApiDownload::EMPTY_REENCODE_FORMAT();
        }

        if (empty($parameter_format)) {
            return DownloaderTierApiDownload::EMPTY_FORMAT();
        }

        // get info loop
        if (!$link->haveInfo()) {
            if ($CLImate instanceof CLImate) $CLImate->out('Get infos ...');
            $timout = 0;
            $limit = 5;

            do {
                $task = GetInfo::getTask($link->getUrlString());
                $status = getStatusOfATask($task);
                $haveInfo = $link->haveInfo();

                if ($status->getValue() === GearmanStatus::UNKNOWN && !$haveInfo) {
                    GetInfo::callJob($link->getUrlString(), true);
                }

                if (!$haveInfo) {
                    if ($CLImate instanceof CLImate) $CLImate->out('Sleep... ' . ($timout + 1) . '/' . $limit);
                    $timout++;
                    sleep(3);
                }
            } while (!$haveInfo && $timout < $limit);

            if ($timout === $limit) {
                if ($CLImate instanceof CLImate) $CLImate->out('Timeout!');
                return DownloaderTierApiDownload::TIMEOUT_GETTING_INFO();
            }

            if ($CLImate instanceof CLImate) $CLImate->out('Get infos OK!');
        }

        $file_path = $link->getVideoMp4DownloadPath($quality, $type);
        $user_agent = UserAgent::getRandomUserAgent();
        $cmd = implode(' ', [
            self::BIN,
            $link->getUrlString(),
            '--user-agent "' . $user_agent . '"',
            '--format "' . $parameter_format . '"',
            $parameter_recode_video,
            '--output ' . $file_path,
            '2>> ' . $error_log_file,
        ]);

        if (!file_exists($error_log_file)) {
            make_dir($error_log_file);
            file_put_contents($error_log_file, '');
        }

        if ($CLImate instanceof CLImate) $CLImate->out($cmd);
        make_dir($file_path);
        exec($cmd, $out, $code);

        if ($CLImate instanceof CLImate) {
            $CLImate->out('Code: ' . $code);
        }

        return $code === 0 ?
            DownloaderTierApiDownload::OK() :
            DownloaderTierApiDownload::DOWNLOADER_TIER_ERROR();
    }

}
