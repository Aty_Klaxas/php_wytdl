<?php declare(strict_types=1);

namespace src;

use Net_Gearman_Task;
use src\Enum\MemcacheResultCode;

class JobsTracker
{

    public static function getHash(string $job, array $arguments): string
    {
        return hash('sha1', self::class . json_encode([
            '$job' => $job,
            '$arguments' => $arguments,
        ]));
    }

    public static function setTask(string $job, array $arguments, Net_Gearman_Task $task): MemcacheResult
    {
        return Memcache::set(self::getHash($job, $arguments), $task);
    }

    public static function getTask(string $job, array $arguments): ?Net_Gearman_Task
    {
        $memcache_result = Memcache::get(self::getHash($job, $arguments));

        if ($memcache_result->getResultCode()->getValue() !== MemcacheResultCode::RES_SUCCESS) {
            return null;
        }

        $task = $memcache_result->getData();

        if (!$task instanceof Net_Gearman_Task) {
            return null;
        }

        return $task;
    }

}
