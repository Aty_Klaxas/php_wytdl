<?php declare(strict_types=1);

namespace src\Enum;

/**
 * @method static self AUDIO()
 * @method static self VIDEO()
 */
class TypeSimpleSelection extends EnumParent
{

    public const AUDIO = 'AUDIO';
    public const VIDEO = 'VIDEO';

}
