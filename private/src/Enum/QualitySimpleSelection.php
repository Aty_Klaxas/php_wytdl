<?php declare(strict_types=1);

namespace src\Enum;

/**
 * @method static self DEFAULT()
 * @method static self LOW()
 * @method static self MEDIUM()
 * @method static self HIGH()
 * @method static self VERY_HIGH()
 */
class QualitySimpleSelection extends EnumParent
{

    public const DEFAULT = 'DEFAULT';
    public const LOW = 'LOW';
    public const LOW_PLUS = 'LOW_PLUS';
    public const MEDIUM = 'MEDIUM';
    public const HIGH = 'HIGH';
    public const VERY_HIGH = 'VERY_HIGH';

}
