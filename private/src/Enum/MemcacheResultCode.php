<?php declare(strict_types=1);

namespace src\Enum;

/**
 * @method static self RES_SUCCESS()
 * @method static self RES_FAILURE()
 * @method static self RES_HOST_LOOKUP_FAILURE()
 * @method static self RES_UNKNOWN_READ_FAILURE()
 * @method static self RES_PROTOCOL_ERROR()
 * @method static self RES_CLIENT_ERROR()
 * @method static self RES_SERVER_ERROR()
 * @method static self RES_WRITE_FAILURE()
 * @method static self RES_DATA_EXISTS()
 * @method static self RES_NOTSTORED()
 * @method static self RES_NOTFOUND()
 * @method static self RES_PARTIAL_READ()
 * @method static self RES_SOME_ERRORS()
 * @method static self RES_NO_SERVERS()
 * @method static self RES_END()
 * @method static self RES_ERRNO()
 * @method static self RES_BUFFERED()
 * @method static self RES_TIMEOUT()
 * @method static self RES_BAD_KEY_PROVIDED()
 * @method static self RES_STORED()
 * @method static self RES_DELETED()
 * @method static self RES_STAT()
 * @method static self RES_ITEM()
 * @method static self RES_NOT_SUPPORTED()
 * @method static self RES_FETCH_NOTFINISHED()
 * @method static self RES_SERVER_MARKED_DEAD()
 * @method static self RES_UNKNOWN_STAT_KEY()
 * @method static self RES_INVALID_HOST_PROTOCOL()
 * @method static self RES_MEMORY_ALLOCATION_FAILURE()
 * @method static self RES_E2BIG()
 * @method static self RES_KEY_TOO_BIG()
 * @method static self RES_SERVER_TEMPORARILY_DISABLED()
 * @method static self RES_SERVER_MEMORY_ALLOCATION_FAILURE()
 * @method static self RES_AUTH_PROBLEM()
 * @method static self RES_AUTH_FAILURE()
 * @method static self RES_AUTH_CONTINUE()
 * @method static self RES_CONNECTION_FAILURE()
 * @method static self RES_CONNECTION_BIND_FAILURE()
 * @method static self RES_READ_FAILURE()
 * @method static self RES_DATA_DOES_NOT_EXIST()
 * @method static self RES_VALUE()
 * @method static self RES_FAIL_UNIX_SOCKET()
 * @method static self RES_NO_KEY_PROVIDED()
 * @method static self RES_INVALID_ARGUMENTS()
 * @method static self RES_PARSE_ERROR()
 * @method static self RES_PARSE_USER_ERROR()
 * @method static self RES_DEPRECATED()
 * @method static self RES_IN_PROGRESS()
 * @method static self RES_MAXIMUM_RETURN()
 * @method static self RES_CONNECTION_SOCKET_CREATE_FAILURE()
 * @method static self RES_PAYLOAD_FAILURE()
 */
class MemcacheResultCode extends EnumParent
{

    /**
     * <p>The operation was successful.</p>
     * @link https://php.net/manual/en/memcached.constants.php
     */
    public const RES_SUCCESS = 0;

    /**
     * <p>The operation failed in some fashion.</p>
     * @link https://php.net/manual/en/memcached.constants.php
     */
    public const RES_FAILURE = 1;

    /**
     * <p>DNS lookup failed.</p>
     * @link https://php.net/manual/en/memcached.constants.php
     */
    public const RES_HOST_LOOKUP_FAILURE = 2;

    /**
     * <p>Failed to read network data.</p>
     * @link https://php.net/manual/en/memcached.constants.php
     */
    public const RES_UNKNOWN_READ_FAILURE = 7;

    /**
     * <p>Bad command in memcached protocol.</p>
     * @link https://php.net/manual/en/memcached.constants.php
     */
    public const RES_PROTOCOL_ERROR = 8;

    /**
     * <p>Error on the client side.</p>
     * @link https://php.net/manual/en/memcached.constants.php
     */
    public const RES_CLIENT_ERROR = 9;

    /**
     * <p>Error on the server side.</p>
     * @link https://php.net/manual/en/memcached.constants.php
     */
    public const RES_SERVER_ERROR = 10;

    /**
     * <p>Failed to write network data.</p>
     * @link https://php.net/manual/en/memcached.constants.php
     */
    public const RES_WRITE_FAILURE = 5;

    /**
     * <p>Failed to do compare-and-swap: item you are trying to store has been
     * modified since you last fetched it.</p>
     * @link https://php.net/manual/en/memcached.constants.php
     */
    public const RES_DATA_EXISTS = 12;

    /**
     * <p>Item was not stored: but not because of an error. This normally
     * means that either the condition for an "add" or a "replace" command
     * wasn't met, or that the item is in a delete queue.</p>
     * @link https://php.net/manual/en/memcached.constants.php
     */
    public const RES_NOTSTORED = 14;

    /**
     * <p>Item with this key was not found (with "get" operation or "cas"
     * operations).</p>
     * @link https://php.net/manual/en/memcached.constants.php
     */
    public const RES_NOTFOUND = 16;

    /**
     * <p>Partial network data read error.</p>
     * @link https://php.net/manual/en/memcached.constants.php
     */
    public const RES_PARTIAL_READ = 18;

    /**
     * <p>Some errors occurred during multi-get.</p>
     * @link https://php.net/manual/en/memcached.constants.php
     */
    public const RES_SOME_ERRORS = 19;

    /**
     * <p>Server list is empty.</p>
     * @link https://php.net/manual/en/memcached.constants.php
     */
    public const RES_NO_SERVERS = 20;

    /**
     * <p>End of result set.</p>
     * @link https://php.net/manual/en/memcached.constants.php
     */
    public const RES_END = 21;

    /**
     * <p>System error.</p>
     * @link https://php.net/manual/en/memcached.constants.php
     */
    public const RES_ERRNO = 26;

    /**
     * <p>The operation was buffered.</p>
     * @link https://php.net/manual/en/memcached.constants.php
     */
    public const RES_BUFFERED = 32;

    /**
     * <p>The operation timed out.</p>
     * @link https://php.net/manual/en/memcached.constants.php
     */
    public const RES_TIMEOUT = 31;

    /**
     * <p>Bad key.</p>
     * @link https://php.net/manual/en/memcached.constants.php, http://docs.libmemcached.org/index.html
     */
    /**
     * <p>MEMCACHED_BAD_KEY_PROVIDED: The key provided is not a valid key.</p>
     */
    public const RES_BAD_KEY_PROVIDED = 33;

    /**
     * <p>MEMCACHED_STORED: The requested object has been successfully stored on the server.</p>
     */
    public const RES_STORED = 15;

    /**
     * <p>MEMCACHED_DELETED: The object requested by the key has been deleted.</p>
     */
    public const RES_DELETED = 22;

    /**
     * <p>MEMCACHED_STAT: A “stat” command has been returned in the protocol.</p>
     */
    public const RES_STAT = 24;

    /**
     * <p>MEMCACHED_ITEM: An item has been fetched (this is an internal error only).</p>
     */
    public const RES_ITEM = 25;

    /**
     * <p>MEMCACHED_NOT_SUPPORTED: The given method is not supported in the server.</p>
     */
    public const RES_NOT_SUPPORTED = 28;

    /**
     * <p>MEMCACHED_FETCH_NOTFINISHED: A request has been made, but the server has not finished the fetch of the last request.</p>
     */
    public const RES_FETCH_NOTFINISHED = 30;

    /**
     * <p>MEMCACHED_SERVER_MARKED_DEAD: The requested server has been marked dead.</p>
     */
    public const RES_SERVER_MARKED_DEAD = 35;

    /**
     * <p>MEMCACHED_UNKNOWN_STAT_KEY: The server you are communicating with has a stat key which has not be defined in the protocol.</p>
     */
    public const RES_UNKNOWN_STAT_KEY = 36;

    /**
     * <p>MEMCACHED_INVALID_HOST_PROTOCOL: The server you are connecting too has an invalid protocol. Most likely you are connecting to an older server that does not speak the binary protocol.</p>
     */
    public const RES_INVALID_HOST_PROTOCOL = 34;

    /**
     * <p>MEMCACHED_MEMORY_ALLOCATION_FAILURE: An error has occurred while trying to allocate memory.</p>
     */
    public const RES_MEMORY_ALLOCATION_FAILURE = 17;

    /**
     * <p>MEMCACHED_E2BIG: Item is too large for the server to store.</p>
     */
    public const RES_E2BIG = 37;

    /**
     * <p>MEMCACHED_KEY_TOO_BIG: The key that has been provided is too large for the given server.</p>
     */
    public const RES_KEY_TOO_BIG = 39;

    /**
     * <p>MEMCACHED_SERVER_TEMPORARILY_DISABLED</p>
     */
    public const RES_SERVER_TEMPORARILY_DISABLED = 47;

    /**
     * <p>MEMORY_ALLOCATION_FAILURE: An error has occurred while trying to allocate memory.
     *
     * #if defined(LIBMEMCACHED_VERSION_HEX) && LIBMEMCACHED_VERSION_HEX >= 0x01000008</p>
     */
    public const RES_SERVER_MEMORY_ALLOCATION_FAILURE = 48;

    /**
     * <p>MEMCACHED_AUTH_PROBLEM: An unknown issue has occured during authentication.</p>
     */
    public const RES_AUTH_PROBLEM = 40;

    /**
     * <p>MEMCACHED_AUTH_FAILURE: The credentials provided are not valid for this server.</p>
     */
    public const RES_AUTH_FAILURE = 41;

    /**
     * <p>MEMCACHED_AUTH_CONTINUE: Authentication has been paused.</p>
     */
    public const RES_AUTH_CONTINUE = 42;

    /**
     * <p>MEMCACHED_CONNECTION_FAILURE: A unknown error has occured while trying to connect to a server.</p>
     */
    public const RES_CONNECTION_FAILURE = 3;

    /**
     * MEMCACHED_CONNECTION_BIND_FAILURE: We were not able to bind() to the socket.
     */
    #[Deprecated('Deprecated since version 0.30(libmemcached)')]
    public const RES_CONNECTION_BIND_FAILURE = 4;

    /**
     * <p>MEMCACHED_READ_FAILURE: A read failure has occurred.</p>
     */
    public const RES_READ_FAILURE = 6;

    /**
     * <p>MEMCACHED_DATA_DOES_NOT_EXIST: The data requested with the key given was not found.</p>
     */
    public const RES_DATA_DOES_NOT_EXIST = 13;

    /**
     * <p>MEMCACHED_VALUE: A value has been returned from the server (this is an internal condition only).</p>
     */
    public const RES_VALUE = 23;

    /**
     * <p>MEMCACHED_FAIL_UNIX_SOCKET: A connection was not established with the server via a unix domain socket.</p>
     */
    public const RES_FAIL_UNIX_SOCKET = 27;

    /**
     * No key was provided.</p>
     */
    #[Deprecated('Deprecated since version 0.30 (libmemcached). Use MEMCACHED_BAD_KEY_PROVIDED instead.')]
    public const RES_NO_KEY_PROVIDED = 29;

    /**
     * <p>MEMCACHED_INVALID_ARGUMENTS: The arguments supplied to the given function were not valid.</p>
     */
    public const RES_INVALID_ARGUMENTS = 38;

    /**
     * <p>MEMCACHED_PARSE_ERROR: An error has occurred while trying to parse the configuration string. You should use memparse to determine what the error was.</p>
     */
    public const RES_PARSE_ERROR = 43;

    /**
     * <p>MEMCACHED_PARSE_USER_ERROR: An error has occurred in parsing the configuration string.</p>
     */
    public const RES_PARSE_USER_ERROR = 44;

    /**
     * <p>MEMCACHED_DEPRECATED: The method that was requested has been deprecated.</p>
     */
    public const RES_DEPRECATED = 45;

    //unknow
    public const RES_IN_PROGRESS = 46;

    /**
     * <p>MEMCACHED_MAXIMUM_RETURN: This in an internal only state.</p>
     */
    public const RES_MAXIMUM_RETURN = 49;

    /**
     * <p>Failed to create network socket.</p>
     * @link https://php.net/manual/en/memcached.constants.php
     */
    public const RES_CONNECTION_SOCKET_CREATE_FAILURE = 11;

    /**
     * <p>Payload failure: could not compress/decompress or serialize/unserialize the value.</p>
     * @link https://php.net/manual/en/memcached.constants.php
     */
    public const RES_PAYLOAD_FAILURE = -1001;

}
