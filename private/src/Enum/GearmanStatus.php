<?php declare(strict_types=1);

namespace src\Enum;

/**
 * @method static self UNKNOWN()
 * @method static self IN_QUEUE()
 * @method static self RUNNING()
 */
class GearmanStatus extends EnumParent
{

    /** @var string Not in job-gearman-server yet OR job ended */
    public const UNKNOWN = 'UNKNOWN';
    public const IN_QUEUE = 'IN_QUEUE';
    public const RUNNING = 'RUNNING';

}
