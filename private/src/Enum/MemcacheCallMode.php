<?php declare(strict_types=1);

namespace src\Enum;

/**
 * @method static self GET()
 * @method static self SET()
 */
class MemcacheCallMode extends EnumParent
{

    public const SET = 'SET';
    public const GET = 'GET';

}
