<?php declare(strict_types=1);

namespace src\Enum;

/**
 * @method static self OK()
 * @method static self INVALID_LINK()
 * @method static self EMPTY_FORMAT()
 * @method static self EMPTY_REENCODE_FORMAT()
 * @method static self DOWNLOADER_TIER_ERROR()
 */
class DownloaderTierApiDownload extends EnumParent
{

    public const OK = 'OK';
    public const INVALID_LINK = 'INVALID_LINK';
    public const EMPTY_FORMAT = 'EMPTY_FORMAT';
    public const EMPTY_REENCODE_FORMAT = 'EMPTY_REENCODE_FORMAT';
    public const DOWNLOADER_TIER_ERROR = 'DOWNLOADER_TIER_ERROR';
    public const TIMEOUT_GETTING_INFO = 'TIMEOUT_GETTING_INFO';

}
