<?php declare(strict_types = 1);

namespace src\Enum;

use ReflectionClass;

/**
 * Dirty
 */
class EnumParent
{

    protected $value = null;

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value): void
    {
        if (in_array($value, self::getAll(), true)) {
            $this->value = $value;
        }
    }

    public function __construct($value = null)
    {
        if ($value instanceof static) {
            $value = $value->getValue();
        }

        $this->setValue($value);
    }

    /**
     * Permit to use a const name as function and return instance of enum
     */
    public static function __callStatic(string $name, array $arguments = [])
    {
        $reflection = new ReflectionClass(static::class);
        $constants = $reflection->getConstants();

        if (empty($constants[$name])) {
            return null;
        }

        return new static($reflection->getConstant($name));
    }

    public static function getAll(): array
    {
        $result = [];
        $ref = new ReflectionClass(static::class);

        foreach($ref->getConstants() as $name => $value) {
            $result[$name] = $value;
        }

        return $result;
    }

    /**
     * Can Throw an error !
     */
    public function __toString(): string
    {
        return (string) $this->value;
    }

}
