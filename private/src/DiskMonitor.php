<?php

namespace src;

class DiskMonitor
{

    public static function getDiskUsage()
    {
        exec('df /', $out);

        array_shift($out);
        $line = array_shift($out);
        $exp = explode(' ', $line);
        $exp = array_filter($exp);
        $exp = array_values($exp);

        $exp[4] = (int) substr($exp[4], 0, -1);

        return array_combine([
            'Filesystem',
            '1K-blocks',
            'Used',
            'Available',
            'Use%',
            'Mounted on',
        ], $exp);
    }

    public static function isAlarmingUsage(): bool
    {
        return self::getDiskUsage()['Use%'] > 85;
    }

}
