<?php declare(strict_types=1);

namespace src;

use League\CLImate\Util\Writer\WriterInterface;

class WorkerLogNShellWriter extends LogNShellWriter
{

    protected string $job;
    protected string $instance;

    public function __construct(string $job, string $instance = '')
    {
        // $job_clean = str_replace('\\', '_', $job);
        // parent::__construct(self::LOG_PATH . '/workers/' . $job_clean . '/worker_' . $job_clean . '_' . $instance . '.log');
        parent::__construct(self::LOG_PATH . '/workers.log');

        $this->job = $job;
        $this->instance = $instance;
    }

    public function write($content)
    {
        $job_n_instance = '[' . $this->job;

        if (!empty($this->instance)) {
            $job_n_instance .= ' ' . $this->instance;
        }

        parent::write($job_n_instance . '] ' . $content);
    }

}
