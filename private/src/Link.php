<?php declare(strict_types=1);

namespace src;

use src\Enum\QualitySimpleSelection;
use src\Enum\TypeSimpleSelection;
use Wa72\Url\Url;

class Link {

    public const PATH_INFO = __DIR__ . '/../../infos';

    protected Url $url;

    public function setUrl(Url $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function setUrlString(string $url): self
    {
        $this->setUrl(new Url($url));

        return $this;
    }

    public function getUrlString(): string
    {
        return $this->url->write();
    }

    public function getUrl(): Url
    {
        return clone $this->url;
    }

    public function getHash(): string
    {
        return hash('md5', $this->getUrlString());
    }


    public function getInfoPath(): string
    {
        $hash = $this->getHash();

        return self::PATH_INFO . '/' . substr($hash, 0, 2) . '/' . $hash . '.json';
    }

    public function haveInfo(): bool
    {
        return file_exists($this->getInfoPath());
    }

    public function getInfoArray(): ?array
    {
        if (!$this->haveInfo()) {
            return null;
        }

        $json = file_get_contents($this->getInfoPath());

        return json_decode($json, true);
    }


    public function getCommonDownloadRelativePath(): ?string
    {
        $hash = $this->getHash();
        return '/download/' . substr($hash, 0, 2) . '/' . $hash;
    }

    public function getCommonDownloadPath(): ?string
    {
        return __DIR__ . '/../../public' . $this->getCommonDownloadRelativePath();
    }

    public function getCommonDownloadLink(): ?string
    {
        return BASE_URL . $this->getCommonDownloadRelativePath();
    }


    public function getVideoMp4Filename(QualitySimpleSelection $quality, TypeSimpleSelection $type): ?string
    {
        if (!$this->haveInfo()) {
            return null;
        }

        $info = $this->getInfoArray();
        $file = str_replace([' ', "'", '(', ')', '@'], '_', ($info['uploader_id'] ?? null) . '-' . ($info['fulltitle'] ?? $info['title'] ?? null));

        if ($type->getValue() === TypeSimpleSelection::AUDIO) {
            $file .= '.mp3';
        } elseif ($type->getValue() === TypeSimpleSelection::VIDEO) {
            $file .= '.mp4';
        } else {
            return null;
        }

        return $type->getValue() . '-' . $quality->getValue() . '-' . $file;
    }

    public function getVideoMp4DownloadPath(QualitySimpleSelection $quality, TypeSimpleSelection $type): ?string
    {
        return $this->getCommonDownloadPath() . '/' . $this->getVideoMp4Filename($quality, $type);
    }

    public function getVideoMp4DownloadLink(QualitySimpleSelection $quality, TypeSimpleSelection $type): ?string
    {
        if (!$this->isVideoMp4Downloaded($quality, $type)) {
            return null;
        }

        return $this->getCommonDownloadLink() . '/' . $this->getVideoMp4Filename($quality, $type);
    }

    public function isVideoMp4Downloaded(QualitySimpleSelection $quality, TypeSimpleSelection $type): bool
    {
        return file_exists($this->getVideoMp4DownloadPath($quality, $type));
    }

}
