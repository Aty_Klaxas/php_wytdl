<?php declare(strict_types=1);

namespace src;

use src\Enum\MemcacheResultCode;
use src\Enum\MemcacheCallMode;

class MemcacheResult
{

    protected $data;
    protected string $key;
    protected MemcacheResultCode $result_code;
    protected string $result_message;
    protected MemcacheCallMode $mode;

    public function getResultCode(): MemcacheResultCode
    {
        return $this->result_code;
    }

    public function setResultCode(MemcacheResultCode $result_code): void
    {
        $this->result_code = $result_code;
    }

    public function getResultMessage(): string
    {
        return $this->result_message;
    }

    public function setResultMessage(string $result_message): void
    {
        $this->result_message = $result_message;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function setKey(string $key): void
    {
        $this->key = $key;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data): void
    {
        $this->data = $data;
    }

    public function getMode(): MemcacheCallMode
    {
        return $this->mode;
    }

    public function setMode(MemcacheCallMode $mode): void
    {
        $this->mode = $mode;
    }

}
