<?php

namespace src;

/**
 * Can be better
 */
class Response
{

    public function json($content): string
    {
        return json_encode($content, JSON_PRETTY_PRINT);
    }

    public function send_error_n_exit(string $error)
    {
        header('Content-Type: application/json');
        exit($this->json([
            'error' => $error,
            'data' => null,
        ]) . PHP_EOL);
    }

    public function send_ok_n_data_n_exit($data = null)
    {
        header('Content-Type: application/json');
        exit($this->json([
            'error' => 'OK',
            'data' => $data,
        ]) . PHP_EOL);
    }

}
