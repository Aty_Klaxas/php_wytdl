<?php declare(strict_types=1);

namespace src;

use src\Enum\MemcacheResultCode;
use src\Enum\MemcacheCallMode;
use Memcached;

class Memcache
{

    public const HOST = '127.0.0.1';
    public const PORT = 11211;

    protected static ?Memcached $memcached = null;

    protected static function init()
    {
        if (!self::$memcached instanceof Memcached) {
            self::$memcached = new Memcached();
            self::$memcached->addServer(self::HOST, self::PORT);
        }
    }

    public static function get(string $key): MemcacheResult
    {
        self::init();
        $data = self::$memcached->get($key);

        $return = new MemcacheResult();
        $return->setMode(MemcacheCallMode::GET());
        $return->setKey($key);
        $return->setData($data);
        $return->setResultCode(new MemcacheResultCode(self::$memcached->getResultCode()));
        $return->setResultMessage(self::$memcached->getResultMessage());

        return $return;
    }

    public static function set(string $key, $data): MemcacheResult
    {
        self::init();
        self::$memcached->set($key, $data);

        $return = new MemcacheResult();
        $return->setMode(MemcacheCallMode::SET());
        $return->setKey($key);
        $return->setData($data);
        $return->setResultCode(new MemcacheResultCode(self::$memcached->getResultCode()));
        $return->setResultMessage(self::$memcached->getResultMessage());

        return $return;
    }

    /**
     * @param string[] $array
     * @return MemcacheResult[]
     */
    public static function getMulti(array $array): array
    {
        return array_map(self::class . '::get', array_combine($array, $array));
    }

    /**
     * @param string[] $array
     * @return MemcacheResult[]
     */
    public static function setMulti(array $array): array
    {
        $return = [];

        foreach ($array as $key => $data) {
            $return[$key] = self::set($key, $data);
        }

        return $return;
    }

}
