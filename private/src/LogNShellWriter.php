<?php declare(strict_types=1);

namespace src;

use Cassandra\Date;
use League\CLImate\Util\Writer\WriterInterface;

class LogNShellWriter implements WriterInterface
{

    public const LOG_PATH = __DIR__ . '/../../log';

    protected string $log_path;

    public function getLogPath(): string
    {
        return $this->log_path;
    }

    public function __construct(string $file)
    {
        $this->log_path = $file;
        make_dir($this->log_path);
    }

    public function write($content)
    {
        $datetime = '[' . (new \DateTime())->format('Y-m-d H:i:s.u') . ']';
        $content = $datetime . ' ' . $content;

        echo $content;
        $content = str_replace('', '', $content);
        $content = str_replace('[m', '', $content);
        $content = str_replace('[0m', '', $content);
        file_put_contents($this->log_path, $content, FILE_APPEND);
    }

}
