<?php declare(strict_types=1);

use Wa72\Url\Url;
use GearmanJobs\GearmanHelper;
use src\Enum\GearmanStatus;
use src\Link;
use src\Response;

function see($val, string $text = '')
{
    echo '############ ' . $text . ' ############<br><pre>' . PHP_EOL;
    var_dump($val);
    echo '</pre>' . PHP_EOL;
}

const VERIFY_URL_OK = 'OK';
const HOST_WHITELIST = [
    'www.youtube.com',
    'youtu.be',
];

/**
 * Todo: create a enum
 *
 * @param $url
 * @return string
 */
function verify_url($url)
{
    if ($url === null) {
        return 'Url is null';
    }

    if (empty($url)) {
        return 'No url';
    }

    if (!is_string($url)) {
        return 'Not string';
    }

    try {
        $Url = new Url($url);
    } catch (Throwable $t) {
        return 'Exception: ' . $t->getMessage();
    }

    if (!$Url->is_url()) {
        return 'Not an url';
    }

    if ($Url->getScheme() !== 'https') {
        return 'Not https';
    }

    if (!empty($Url->getUser())) {
        return 'Have user';
    }

    if (!empty($Url->getPass())) {
        return 'Have pass';
    }

    if (!empty($Url->getPort())) {
        return 'Have port';
    }

    if (!in_array($Url->getHost(), HOST_WHITELIST, true)) {
        return 'Hot whitelisted';
    }

    return VERIFY_URL_OK;
}

function create_youtube_url(string $vid): Url
{
    return (new Url(''))
        ->setScheme('https')
        ->setHost('www.youtube.com')
        ->setPath('/watch')
        ->setQueryParameter('v', $vid)
    ;
}

function extract_url(Url $Url): ?string
{
    if ($Url->getHost() === 'www.youtube.com') {
        $vid = $Url->getQueryParameter('v');

        return create_youtube_url($vid)->write();
    }

    if ($Url->getHost() === 'youtu.be') {
        $path = $Url->getPath();
        $vid = substr($path, 1);

        return create_youtube_url($vid)->write();
    }

    return null;
}

function make_dir(string $full_path)
{
    $dir = pathinfo($full_path, PATHINFO_DIRNAME);

    if (!is_dir($dir)) {
        mkdir($dir, 0777, true);
    }

    return is_dir($dir);
}

function make_json_pretty_print(string $json_file): bool
{
    try {
        $content = file_get_contents($json_file);
        $json = json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        $re_content = json_encode($json, JSON_PRETTY_PRINT);
        file_put_contents($json_file, $re_content);
    } catch (Throwable $t) {
        return false;
    }

    return true;
}

/**
 * @param $url
 * @return string|Url
 */
function verify_and_re_verify_url($url)
{
    $error = verify_url($url);

    if ($error !== VERIFY_URL_OK) {
        return 'Pre extract: ' . $error;
    }

    $url = extract_url(new Url($url));
    $error = verify_url($url);

    if ($error !== VERIFY_URL_OK) {
        return 'Post extract: ' . $error;
    }

    return new Url($url);
}

/**
 * If you need to get multiple task status, use GearmanHelper::getStatus($tasks);
 *
 * @param Net_Gearman_Task $task
 * @throws Net_Gearman_Exception
 */
function getStatusOfATask($task): GearmanStatus
{
    if (!$task instanceof Net_Gearman_Task) {
        return new GearmanStatus();
    }

    $status = GearmanHelper::getStatus($task);
    $status = array_shift($status);

    if (!$status instanceof GearmanStatus) {
        return new GearmanStatus();
    }

    return $status;
}

/**
 * This function can exit
 */
function parameter_url_to_link(): Link
{
    $url = $_GET['url'] ?? null;
    $Response = new Response();
    $error = verify_and_re_verify_url($url);

    if (!$error instanceof Url) {
        $Response->send_error_n_exit($error);
    }

    $url = extract_url(new Url($url));
    $Link = new Link();
    $Link->setUrlString($url);

    return $Link;
}
