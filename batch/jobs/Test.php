<?php declare(strict_types=1);

namespace GearmanJobs;

use src\JobsTracker;
use src\Link;

class Test extends ParentJob
{

    public static function callJob(Link $link, int $id, bool $async = false) {
        $arguments = ['link' => $link->getUrlString(), 'id' => $id];
        $task = GearmanHelper::callJob(static::class, $arguments, $async);

        if ($task instanceof \Net_Gearman_Task && $async === true) {
            JobsTracker::setTask(static::class, $arguments, $task);
        }

        return $task;
    }

    public static function getTask(Link $link, int $id) {
        return JobsTracker::getTask(static::class, ['link' => $link, 'id' => $id]);
    }

    public function doJob($arguments)
    {
        $this->log->out('Arguments: ' . json_encode($arguments));

        // simulate latencies
        $this->log->out('Sleep 10 sec...');
        sleep(10);

        return $this->log_n_return('test_' . microtime(true));
    }

}
