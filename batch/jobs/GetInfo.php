<?php declare(strict_types=1);

namespace GearmanJobs;

use src\DiskMonitor;
use src\JobsTracker;
use src\Link;
use src\DownloaderTierApi;
use Wa72\Url\Url;

class GetInfo extends ParentJob
{

    public static function callJob(string $url, bool $async = false) {
        return parent::callJob(['url' => $url], $async);
    }

    public static function getTask(string $url) {
        return parent::getTask(['url' => $url]);
    }

    public function doJob($arguments)
    {
        if (!file_exists(DownloaderTierApi::BIN)) {
            return $this->log_n_return('ERROR: Bin path does not exist');
        }

        if (!is_file(DownloaderTierApi::BIN)) {
            return $this->log_n_return('ERROR: Bin path is not a file');
        }

        if (empty($arguments['url'])) {
            return $this->log_n_return('ERROR: Empty url parameter');
        }

        $url = $arguments['url'];
        $error = verify_url($url);

        if ($error !== VERIFY_URL_OK) {
            return $this->log_n_return('ERROR: ' . $error);
        }

        $real_url = extract_url(new Url($url));
        $error = verify_url($real_url);

        if ($error !== VERIFY_URL_OK) {
            return $this->log_n_return('ERROR: Post extract_url: ' . $error);
        }

        $Link = new Link();
        $Link->setUrlString($real_url);

        if (DiskMonitor::isAlarmingUsage()) {
            return $this->log_n_return('ERROR: alarming usage of the disk');
        }

        if (!$Link->haveInfo()) {
            $this->log->out('Get info... ' . $real_url);

            if (!DownloaderTierApi::getInfoDumpSingleJson($Link)) {
                return $this->log_n_return('ERROR: Unable to get info, verification error');
            }

            if (!make_json_pretty_print($Link->getInfoPath())) {
                return $this->log_n_return('ERROR: Unable to make json pretty print');
            }
        } else {
            return $this->log_n_return('Have info');
        }

        return $this->log_n_return('OK');
    }

}
