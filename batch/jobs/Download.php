<?php declare(strict_types=1);

namespace GearmanJobs;

use src\DiskMonitor;
use src\Enum\DownloaderTierApiDownload;
use src\Enum\QualitySimpleSelection;
use src\Enum\TypeSimpleSelection;
use src\Link;
use src\DownloaderTierApi;
use src\WorkerLogNShellWriter;
use Wa72\Url\Url;

class Download extends ParentJob
{

    public static function callJob(string $url, QualitySimpleSelection $quality, TypeSimpleSelection $type, bool $async = false) {
        return parent::callJob([
            'url' => $url,
            'quality' => $quality->getValue(),
            'type' => $type->getValue(),
        ], $async);
    }

    public static function getTask(string $url, QualitySimpleSelection $quality, TypeSimpleSelection $type) {
        return parent::getTask([
            'url' => $url,
            'quality' => $quality->getValue(),
            'type' => $type->getValue(),
        ]);
    }

    public function doJob($arguments)
    {
        if (!file_exists(DownloaderTierApi::BIN)) {
            return $this->log_n_return('ERROR: Bin path does not exist');
        }

        if (!is_file(DownloaderTierApi::BIN)) {
            return $this->log_n_return('ERROR: Bin path is not a file');
        }

        $error = $this->verify_argument($arguments);

        if (!empty($error)) {
            return $error;
        }

        $quality = new QualitySimpleSelection($arguments['quality']);
        $type = new TypeSimpleSelection($arguments['type']);

        $Link = new Link();
        $Link->setUrlString($arguments['url']);

        return $this->doJobWithParameter($Link, $quality, $type);
    }

    public function doJobWithParameter(Link $Link, QualitySimpleSelection $quality, TypeSimpleSelection $type)
    {
        $Url = $Link->getUrl();
        $is_downloaded = $Link->isVideoMp4Downloaded($quality, $type);

        if ($is_downloaded) {
            return $this->log_n_return('Already downloaded');
        }

        if (DiskMonitor::isAlarmingUsage()) {
            return $this->log_n_return('ERROR: alarming usage of the disk');
        }

        $this->log->out('Downloading... ' . $Url->write());
        /** @var WorkerLogNShellWriter $worker_logger */
        $worker_logger = $this->log->output->get('worker_logger');
        $log_path = '';

        if ($worker_logger instanceof WorkerLogNShellWriter) {
            $log_path = $worker_logger->getLogPath();
        }

        $enum_return = DownloaderTierApi::download($Link, $quality, $type, $log_path, $this->log);

        if ($enum_return->getValue() === DownloaderTierApiDownload::OK) {
            $this->log->out('Downloaded! ' . $Url->write());
        } else {
            $this->log->out(DownloaderTierApi::class . ' ERROR! (' . $quality->getValue() . ' ' . $type->getValue() . ' ' . $Url->write() . ') Error: ' . $enum_return->getValue());
        }

        return $this->log_n_return('OK');
    }

    public function verify_argument(array $arguments): ?string
    {
        // here or not
        if (empty($arguments['url'])) {
            return $this->log_n_return('ERROR: Empty url parameter');
        }

        if (empty($arguments['quality'])) {
            return $this->log_n_return('ERROR: Empty quality parameter');
        }

        if (empty($arguments['type'])) {
            return $this->log_n_return('ERROR: Empty type parameter');
        }

        $quality = new QualitySimpleSelection($arguments['quality']);

        if ($quality->getValue() === null) {
            return $this->log_n_return('ERROR: Invalid quality parameter');
        }

        $type = new TypeSimpleSelection($arguments['type']);

        if ($type->getValue() === null) {
            return $this->log_n_return('ERROR: Invalid type parameter');
        }

        $Url = verify_and_re_verify_url($arguments['url']);

        if (is_string($Url)) {
            return $this->log_n_return('ERROR: ' . $Url);
        } elseif (!$Url instanceof Url) {
            return $this->log_n_return('ERROR: Unsupported verify_and_re_verify_url() return');
        }

        return null;
    }

}
