<?php declare(strict_types=1);

namespace GearmanJobs;

use League\CLImate\CLImate;
use Net_Gearman_Connection;
use src\JobsTracker;

abstract class ParentJob extends \Net_Gearman_Job_Common
{

    protected ?CLImate $log = null;

    public function __construct(Net_Gearman_Connection $conn, $handle, array $initParams = array())
    {
        parent::__construct($conn, $handle, $initParams);

        if ($initParams['log'] instanceof CLImate) {
            $this->log = $initParams['log'];
        }
    }

    abstract function doJob($arguments);

    final public function run($arg)
    {
        try {
            $this->log->out('Run');
            $return = $this->doJob($arg);
        } catch (\Throwable $t) {
            return $this->log_n_return('ERROR: Exception thrown: ' . $t->getMessage());
        }

        return $return;
    }

    public function log_n_return(string $log): string
    {
        $this->log->out($log);

        return $log;
    }

    public static function callJob(array $arguments, bool $async = false) {
        $task = GearmanHelper::callJob(static::class, $arguments, $async);

        if ($task instanceof \Net_Gearman_Task && $async === true) {
            JobsTracker::setTask(static::class, $arguments, $task);
        }

        return $task;
    }

    public static function getTask(array $arguments) {
        return JobsTracker::getTask(static::class, $arguments);
    }

}
