<?php declare(strict_types=1);

namespace GearmanJobs;

use Net_Gearman_Client;
use src\Enum\GearmanStatus;
use src\JobsTracker;

class GearmanHelper extends Net_Gearman_Client
{

    public const GEARMAN_SERVER = 'localhost';
    public const GEARMAN_PREFIX = 'gearman_prefix_wytdl_';

    public const CALL_JOB_HIGH = 'CALL_JOB_HIGH';
    public const CALL_JOB_MEDIUM = 'CALL_JOB_MEDIUM';
    public const CALL_JOB_LOW = 'CALL_JOB_LOW';

    public static function createWorker(string $job, array $arguments = [])
    {
        $job_path = implode('', explode('GearmanJobs\\', $job));
        $job_path = str_replace('\\', '/', $job_path);
        $job_path = NET_GEARMAN_JOB_PATH . '/' . $job_path . '.php';

        if (!file_exists($job_path)) {
            throw new \RuntimeException('Error: inexistant file: ' . $job_path);
        }

        $worker = new \Net_Gearman_Worker(self::GEARMAN_SERVER);
        $arguments['path'] = $job_path;
        $arguments['class_name'] = $job;
        $worker->addAbility(self::GEARMAN_PREFIX . $job, null, $arguments);
        $worker->beginWork('stopWorkingOnSignal');
    }

    /**
     * @return \Net_Gearman_Task|mixed
     * @throws \Net_Gearman_Exception
     */
    public static function callJob(string $job, array $arguments  = [], bool $async = false, string $job_level = self::CALL_JOB_MEDIUM)
    {
        if ($job_level === self::CALL_JOB_HIGH) {
            $job_level = $async ? \Net_Gearman_Task::JOB_HIGH_BACKGROUND : \Net_Gearman_Task::JOB_HIGH;
        } elseif ($job_level === self::CALL_JOB_LOW) {
            $job_level = $async ? \Net_Gearman_Task::JOB_LOW_BACKGROUND : \Net_Gearman_Task::JOB_LOW;
        } else {
            $job_level = $async ? \Net_Gearman_Task::JOB_BACKGROUND : \Net_Gearman_Task::JOB_NORMAL;
        }

        $arguments['PRIORITY'] = $job_level;
        $arguments['JOB_ASYNCHRONOUS'] = $async;

        $task = new \Net_Gearman_Task(self::GEARMAN_PREFIX . $job, $arguments, null, $job_level);
        $set = new \Net_Gearman_Set();
        $set->addTask($task);

        $client = new \Net_Gearman_Client(self::GEARMAN_SERVER);
        $client->runSet($set);

        if ($async) {
            return $task;
        }

        return $task->result['result'] ?? null;
    }

    /**
     * @param \Net_Gearman_Task|\Net_Gearman_Task[] $tasks Array of task
     * @throws \Net_Gearman_Exception
     * @return GearmanStatus[]|null
     */
    public static function getStatus($tasks): ?array
    {
        if ($tasks instanceof \Net_Gearman_Task) {
            $tasks = [$tasks];
        } elseif (!is_array($tasks)) {
            return null;
        }

        $tasks = array_filter($tasks, static function($item) {
            return $item instanceof \Net_Gearman_Task;
        });

        if (empty($tasks)) {
            return null;
        }

        // get the client to gearman
        $client = new self(self::GEARMAN_SERVER);
        $conn = $client->getConnection();
        $handle_count = 0;
        $handle_to_key = [];
        $return = [];

        foreach ($tasks as $key => $task) {
            // verify validity
            if (!$task instanceof \Net_Gearman_Task) {
                continue;
            }

            // increment and send command
            $handle_count++;
            $handle_to_key[$task->handle] = $key;
            // command list : Net_Gearman_Connection::$commands
            $conn->send('get_status', ['handle' => $task->handle]);
        }

        // waiting response variables
        $counter = 0;
        $wait_time = 20;

        // call for response $wait_time times
        do {
            // wait logic
            if ($counter !== 0) {
                sleep(1);
            }

            // call $handle_count time data sended
            for ($i = 0; $i < $handle_count; $i++) {
                // read response and get handle
                $response = $conn->read();
                $handle = $response['data']['handle'] ?? null;

                if (empty($handle)) {
                    continue;
                }

                // return by input key
                $return[$handle_to_key[$handle]] = $response['data'];
            }

            // if $gearman_return is !empty, that mean gearman responded
        } while (count($return) !== $handle_count && $counter++ < $wait_time);

        if ($counter >= $wait_time) {
            return null;
        }

        return array_map(static function($item) {
            if (empty($item['known'])) {
                return GearmanStatus::UNKNOWN();
            }

            return empty($item['running']) ?
                GearmanStatus::IN_QUEUE() :
                GearmanStatus::RUNNING();
        }, $return);
    }

}
