#!/bin/bash

# or just cd to the location is the script like __DIR__ in PHP
HERE=$(cd "$(dirname "$BASH_SOURCE")"; cd -P "$(dirname "$(readlink "$BASH_SOURCE" || echo .)")"; pwd)
cd $HERE;

PHPBIN="/usr/bin/php7.4"
CLASS="GearmanJobs\\$1"
INSTANCE_NUMBER=1

if [ -e "configs/$1.config" ]
then
  source configs/$1.config;
fi

echo '$CLASS '$CLASS;
echo '$INSTANCE_NUMBER '$INSTANCE_NUMBER;

for i in $(seq 1 1 $INSTANCE_NUMBER); do
  echo $i;
  $PHPBIN worker.php "$CLASS" "$i"&
done
