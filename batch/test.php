<?php

require_once __DIR__ . '/../private/autoload.php';

use GearmanJobs\Test;
use src\Link;
use src\Enum\GearmanStatus;

$link = new Link();
$link->setUrlString('https://youtu.be/uFAScUG3irU?si=BKJ40W4DZ1wRAyll');
$task = Test::callJob($link, 123, true);

do {
    $status = getStatusOfATask($task);
    echo $status->getValue() . PHP_EOL;
} while($status->getValue() !== GearmanStatus::UNKNOWN);
