<?php

require_once __DIR__ . '/../private/autoload.php';

use GearmanJobs\GearmanHelper;
use GearmanJobs\ParentJob;
use League\CLImate\CLImate;
use src\WorkerLogNShellWriter;

function before_log($identifier = null)
{
    return '[' . (new \DateTime())->format('Y-m-d H:i:s') . '] [worker.php ' . $identifier . '] ';
}

function show_usable_jobs($identifier = null)
{
    echo before_log($identifier) . 'Usable jobs:' . PHP_EOL;
    foreach (get_declared_classes() as $class) {
        if (!is_subclass_of($class, ParentJob::class)) {
            continue;
        }

        echo $class . PHP_EOL;
    }
}

$job = $argv[1] ?? '';
$identifier = $argv[2] ?? '';

$worker_logger = new WorkerLogNShellWriter($job, $identifier);
$climate = new CLImate();
$climate->output->add('worker_logger', $worker_logger);
$climate->output->defaultTo('worker_logger');

if (empty($job)) {
    $climate->out('Empty parameter');
    show_usable_jobs($identifier);
    exit;
}

if (!class_exists($job)) {
    $climate->out('Class ' . $job . ' does not exist');
    show_usable_jobs($identifier);
    exit;
}

if (!is_subclass_of($job, ParentJob::class)) {
    $climate->out('Class ' . $job . ' is not subclass of ' . ParentJob::class);
    show_usable_jobs($identifier);
    exit;
}

$climate->out('Start job: ' . $job);
GearmanHelper::createWorker($job, [
    'log' => $climate,
]);
