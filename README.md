# Web Downloader

This is for linux  
you need php and composer  
based on php7.4  
You will read "downloader" sometimes, you know what it is, I can't say it here (I guess)  
it's about youtube ;)

## Setup backend

We assume you are in the project directory on the server on every commands

### Step 1

1. Install a downloader, php7.4, composer, gearman-job-server and memcached on the system
2. Paste the project somewhere you want, it will take space in this directory for the usage of the app
3. Create working directories `mkdir infos log public/download`

### Step 2 - Php composer

1. `cd private`
2. `composer install`

If you have any error, you can try to fix it yourself  
If error is about memcached, Good Luck.  
I hope you know what you do

### Step 3 - Php consts

1. In `private/src/DownloaderTierApi.php` Change the BIN const by the downloader path
2. In `batch/jobs/GearmanHelper.php` Change the GEARMAN_SERVER const by the ip of gearman job server
3. In `private/src/Memcache.php` change the HOST and PORT consts by memcached host and port
4. In `private/src/DiskMonitor.php` Check if the command `df /` work in terminal, if not, add `return false` in isAlarmingUsage() function at first line to disable disk monitoring

### Step 4 - Jobs

1. Do `chmod +x batch/deamon.sh`
2. In `batch/deamon.sh` Modify the php binary path (variable named PHPBIN) by the system php binary
3. Change the user in `batch/wytdl@.service` by the current user, the service need to be normal user and can read write in precedent created directories
4. Change the path in `batch/wytdl@.service` to full path to deamon.sh file
5. Copy generic service `sudo cp batch/wytdl@.service /etc/systemd/system`
6. Reload systemctl `sudo systemctl daemon-reload`
7. Start the jobs `sudo systemctl start wytdl@GetInfo wytdl@Download`
8. Check if you have log and/or errors in `log` directory

If you want, you can have multiple jobs in parallel in configs file in `batch/configs`  
Just edit INSTANCE_NUMBER to the number you want  
/!\ be careful with this, more job you have, more downloader in parallel you have, more suspect you are /!\  
(i will implement proxy another time)

### Step 5 - Web server config

1. Make a rule in your web server to point on `public` directory
2. In `private/autoload.php`, change the const `BASE_URL` by the endpoint of `public` directory  
   Example: if `public` directory is pointed by `https://mysite.fr/foo/wytdl/bar` I will set this in `BASE_URL`  
   Like that `const BASE_URL = 'https://mysite.fr/foo/wytdl/bar'`  
   that way, I can access to `https://mysite.fr/foo/wytdl/bar/api/status.php`

### Step 6 - test

Test if this worked  
we will define {BASE_URL} as the base url defined earlier  
and {url} as url you want process in the app  
TO EVERY ENDPOINT, pass `debug` parameter to see php errors  
it's possible you have notices

1. Test status first `curl {BASE_URL}/api/status.php?debug&url={url}`
2. Test get_info `curl {BASE_URL}/api/get_info.php?debug&url={url}`
3. Test download `curl {BASE_URL}/api/download.php?debug&url={url}&type=VIDEO`
4. Test download on a different and unknown url of the app before
5. From download request, you have link, test to access it  
   If you can't access to link in download endpoint, it may be caused by the web server or a wrong BASE_URL

In logs, if you have error about disk usage  
Check if `df /` is < 85%  
if you want you can read `private/src/DiskMonitor.php`  
this class is fast way to have disk usage and implement a security to not saturate the disk

### Step 7 - Done !

The backend in OK !
